const {src, dest, series, parallel, watch} = require('gulp'),
    browserSync = require('browser-sync').create(),
    del = require('del'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    uncss = require('gulp-uncss'),
    rename = require("gulp-rename"),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin');
    uglify = require('gulp-uglify-es').default;

function copyHtml(){
    return src('./src/index.html')
        .pipe(dest('./dist'));  // copy index.html to dist folder
}

function copyAssets(){
    return src('./src/assets/images/*.*')
        .pipe(imagemin())
        .pipe(dest('./dist/img'));  // copy assets from src to dist folder
}

function watchChanges() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    watch("./src/*.html").on('change', series(build, browserSync.reload));
    watch("./src/scss/*.scss").on('change', series(build, browserSync.reload));
    watch("./src/js/*.js").on('change', series(build, browserSync.reload));
}

function deleteDist() {
    return del('./dist/**')  // delete dist folder
}

function delUnusedCss() {
    return src('./dist/styles/style.min.css')
        .pipe(uncss({  // delete unused css code
            html: ['./dist/index.html'], ignore: [/\.active/] //mentioned not to delete classes (.active) that will be generated dynamically by JS
        }))
        .pipe(dest('./dist/styles'))
}

function compileCss() {
    return src('./src/scss/style.scss')
        .pipe(sass().on('error', sass.logError, function(e){  // compile *.scss to *.css, if there are any errors they will be printed on log file and console.log.
            console.log(e);
        }))
        .pipe(cleanCSS({compatibility: 'ie8'})) // clean *.css from "spaces"
        .pipe(rename({suffix: '.min'})) // adding to *.css file suffix ".min"
        .pipe(dest('./dist/styles')) // copying result to ./dist/styles
}

function autoPrefixer() {
    return src('dist/styles/style.min.css')
        .pipe(autoprefixer({   // adding prefixes to "styles.min.css" on dist folder
            browsers: ['last 2 versions', '> 1%', 'IE 10'], // mentioned the most popular browsers versions
            cascade: false
        }))
        .pipe(dest('dist/styles'))
}

function concatJS() {
    return src('./src/js/*.js')
        .pipe(concat('script.js'))  // concat js
        .pipe(rename({suffix: '.min'})) // rename js
        .pipe(uglify()) //compress js
        .pipe(dest('./dist/js'));
}

const build = series(deleteDist, parallel(copyHtml, copyAssets), compileCss, concatJS, delUnusedCss, autoPrefixer);
const dev = watchChanges;

exports.dev = dev;
exports.build = build;
exports.default = series(build, dev);
