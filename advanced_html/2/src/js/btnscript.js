const hamburgerButtonWrapper = document.querySelector(".hamburger-button-wrapper");
const hamburgerButton = document.querySelector(".hamburger-button");
const mainMenu = document.querySelector(".header-navigation");

hamburgerButtonWrapper.addEventListener('click', () => {
    hamburgerButton.classList.toggle("active");
    mainMenu.classList.toggle("active");
});

