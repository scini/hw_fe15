class Trello {
    constructor() {
        this._cover = "";
        this._btn = "";
        this._cards = "";
        this._cardsWrapper = "";
        this._createTrelloWrapper();
        this._addButton();
        this._addHeader();
        this._addCardsWrapper();
        this._createSortMethod();
        this._addCardText();
    }

    _createTrelloWrapper() {
        const body = document.querySelector('body');
        this._cover = document.createElement('div');
        this._cover.classList.add('trello');
        body.prepend(this._cover);
    }

    _addHeader() {
        const header = document.createElement('h4');
        header.classList.add('trello__header');
        header.innerText = "To do";
        this._cover.prepend(header);
    }

    _addCardsWrapper() {
        this._cardsWrapper = document.createElement('div');
        this._cardsWrapper.classList.add('trello__cards-wrapper');
        this._btn.before(this._cardsWrapper);
    }

    _createSortMethod() {
        const sortMethod = document.createElement('button');
        sortMethod.classList.add('trello__sort-method');
        sortMethod.innerText = 'Sort by "abc"';
        this._cover.prepend(sortMethod);
        sortMethod.addEventListener('click', this._sortByAbc.bind(this));
    }

     _addCardText() {
        let cardText = document.createElement('textarea');
        cardText.name = 'text';
        cardText.rows = 1;
        cardText.placeholder = 'Enter a new task here';
        cardText.classList.add('trello__card-text');
        cardText.draggable = true;
        this._cardsWrapper.append(cardText);
        cardText.addEventListener('keyup', function () {
            this.style.overflow = 'hidden';
            this.style.height = '0';
            this.style.height = this.scrollHeight + 'px';
        }, false);
        this._dragCard();
    }

    _sortByAbc() {
        let changer = true;
        let shouldSwitchCard = '';
        let i = "";
        this._cards = this._cardsWrapper.getElementsByTagName('textarea');
        while (changer) {
            changer = false;
            for (i = 0; i < (this._cards.length - 1); i++) {
                shouldSwitchCard = false;
                if (this._cards[i].value.toLowerCase() > this._cards[i + 1].value.toLowerCase()) {
                    shouldSwitchCard = true;
                    break;
                }
            }
            if (shouldSwitchCard) {
                this._cards[i].parentNode.insertBefore(this._cards[i + 1], this._cards[i]);
                changer = true;
            }
        }
    }

    _addButton() {
        this._btn = document.createElement('button');
        this._btn.classList.add('trello__add-button');
        this._btn.innerText = 'Add a new task';
        this._cover.append(this._btn);
        this._btn.addEventListener('click', this._addCardText.bind(this));
    }

    _dragCard() {
        this._cards = document.querySelectorAll('.trello__card-text');
        this._cardsWrapper.addEventListener('dragstart', (event) => {
            event.target.classList.add('active');
        });
        this._cardsWrapper.addEventListener('dragend', (event) => {
            event.target.classList.remove('active');
        });
        const getNextCard = (cursorPosition, currentCard) => {
            const currentCardCoord = currentCard.getBoundingClientRect();
            const currentCardCenter = currentCardCoord.y + currentCardCoord.height / 2;
            const nextCard = (cursorPosition < currentCardCenter) ? currentCard : currentCard.nextElementSibling;
            return nextCard;
        };
        this._cardsWrapper.addEventListener('dragover', (event) => {
            event.preventDefault();
            const activeCard = this._cardsWrapper.querySelector('.active');
            const currentCard = event.target;
            const movableCard = activeCard !== currentCard && currentCard.classList.contains('visit-card');
            if (!movableCard) {
                return;
            }
            const nextCard = getNextCard(event.clientY, currentCard);
            if (nextCard && activeCard === nextCard.previousElementSibling || activeCard === nextCard) {
                return;
            }
            this._cardsWrapper.insertBefore(activeCard, nextCard);
        })
    }
}

const toDoList = new Trello();
