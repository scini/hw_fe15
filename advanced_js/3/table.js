class Table {
    constructor(tableWrapper, table, cellsAmount) {
        this.tableContainer = tableWrapper;
        this.table = table;
        this._cellsAmount = cellsAmount;
        this._tableRow = '';
        this._tableCell = '';
        this._createTable();
    }
    _createTable(){
        this.table.className = 'table';
        this.tableContainer.prepend(this.table);
        for (let i = 0; i < this._cellsAmount; i++) {
            this._createRow()
        }
    }

    _createRow(){
        this._tableRow = document.createElement('tr');
        this._tableRow.className = 'table__row';
        this.table.prepend(this._tableRow);
        for (let i = 0; i < this._cellsAmount; i++) {
            this._createColumn();
        }
    }

    _createColumn() {
        this._tableCell = document.createElement('td');
        this._tableCell.className = 'table__column';
        this._tableRow.append(this._tableCell);
    }

}

const tableWrapper = document.querySelector('.game__table-wrapper');
const table = document.createElement('table');
const gameTable = new Table(tableWrapper, table, 10);
