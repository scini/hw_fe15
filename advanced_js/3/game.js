class Game {
    constructor(table, btnStart, btnStop, btnRestart) {
        this.table = table;
        this.btnStart = btnStart;
        this.btnStop = btnStop;
        this.btnRestart = btnRestart;
        this._shuffledList = "";
        this._activeCell = "";
        this._cuttedElement = "";
        this._nextTurn = "";
        this._gameList = "";
        this._cellsList = "";
        this._playersScoreCell = "";
        this._computerScoreCell = "";
        this._counter = 0;
        this._gameTime = 0;
        this._playerScore = 0;
        this._computerScore = 0;
        this._btnListener();
        this._getTurnTime();
        this._tableEventListener();
    }

    _btnListener() {
        this.btnStart.addEventListener('click', this._startGame.bind(this));
        this.btnStop.addEventListener('click', this._stopGame.bind(this));
        this.btnStop.removeEventListener('click', this._stopGame.bind(this));
        this.btnRestart.addEventListener('click', this._restartGame.bind(this));
        this.btnRestart.removeEventListener('click', this._restartGame.bind(this));
    }

    _tableEventListener() {
        this._playersScoreCell = document.querySelector('.game__player-scores');
        this.table.addEventListener('click', (event) => {
            let selectedCell = event.target;
            if (selectedCell.style.background === 'blue') {
                selectedCell.style.background = 'green';
                this._playerScore++;
                this._playersScoreCell.innerText = this._playerScore;
                clearTimeout(this._nextTurn);
                this._nextTurn = setTimeout(this._getRandomCell.bind(this),  this._gameTime);
                if(this._playerScore === 50) {
                    clearTimeout(this._nextTurn);
                    setTimeout(()=> alert('You are winner!'), 700);
                    this.btnRestart.classList.add('active');
                    this.btnStop.classList.remove('active');
                }
            }
        });
    }

    _getTurnTime() {
        this._gameTime = document.querySelector('input[name="timer"]:checked').value;
        switch(this._gameTime) {
            case 1000:
                return 1000;
            case 1500:
                return 1500;
            case 2000:
                return 2000;
        }
        console.log(this._gameTime);
    };

    _startGame() {
        this.btnStart.classList.add('active');
        this.btnStop.classList.add('active');
        this._getTurnTime();
        this._createFieldArray();
        this.btnStart.removeEventListener('click', this._startGame.bind(this));
    }

    _createFieldArray() {
        this._gameList = Array.from({length: 10}, () => Array.from({length: 10}, () => {
            for(let i = 0; i < 100; i++) {
                return this._counter++
            }
        })).flat();
        this._shuffledList = this._gameList.sort(() => Math.random() - .5);
        this._getRandomCell();
    }

    _shiftNextCell() {
        this._compScore = setTimeout(this._userFalse.bind(this),  this._gameTime);
        this._nextTurn = setTimeout(this._getRandomCell.bind(this), this._gameTime);
    }

    _getRandomCell() {
        this._cuttedElement = this._shuffledList.pop();
        this._findTableCell();
        this._shiftNextCell();
    }

    _findTableCell() {
        this._cellsList  = document.querySelectorAll('.table__column');
        this._activeCell = this._cellsList[this._cuttedElement];
        this._activeCell.style.background = 'blue';
        return this._activeCell;
    }

    _userFalse() {
        this._computerScoreCell = document.querySelector('.game__computer-scores');
        if (this._activeCell.style.background === 'blue') {
            this._activeCell.style.background = 'red';
            this._computerScore++;
            this._computerScoreCell.innerText = this._computerScore;
            if(this._computerScore === 50) {
                clearTimeout(this._nextTurn);
                setTimeout(()=> alert('You loose!'), 700);
                this.btnStop.classList.remove('active');
                this.btnRestart.classList.add('active');
            }
        }
    }

    _stopGame(){
        clearTimeout(this._nextTurn);
        setTimeout(()=> {
            alert('You loose!');
            this.btnStop.classList.remove('active');
            this.btnRestart.classList.add('active')
        }, 700);
        this.btnStart.classList.add('active');
    }

    _restartGame(){
        this.btnRestart.classList.remove('active');
        this.btnStop.classList.add('active');
        this._gameList = "";
        if(this._cellsList.length > 0) {
            this._cellsList.forEach(el => {
                el.style.background = ""
            });
        }
        this._playerScore = 0;
        this._playersScoreCell.innerText = 0;
        this._computerScore = 0;
        this._computerScoreCell.innerText = 0;
        this._counter = 0;
        this._startGame();
    }
}

const btnStart = document.querySelector('.game__button.start');
const btnStop = document.querySelector('.game__button.stop');
const btnRestart = document.querySelector('.game__button.restart');
const game = new Game(table, btnStart, btnStop, btnRestart);


