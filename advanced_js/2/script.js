class Hamburger {
    static SIZES = {
        SIZE_SMALL: {
            price: 50,
            calories: 20
        },
        SIZE_LARGE: {
            price: 100,
            calories: 40
        }
    };
    static STUFFINGS = {
        STUFFING_CHEESE: {
            price: 10,
            calories: 20
        },
        STUFFING_SALAD: {
            price: 20,
            calories: 5
        },
        STUFFING_POTATO: {
            price: 15,
            calories: 10
        }
    };
    static TOPPINGS = {
        TOPPING_MAYO: {
            price: 20,
            calories: 5
        },
        TOPPING_SPICE: {
            price: 15,
            calories: 0
        }
    };
    constructor(size, stuffing) {
        // Hamburger size exceptions processing
        if (!size) {
            throw new HamburgerException('no size given')
        } else if (!Hamburger.SIZES.hasOwnProperty(size)) {
            throw new HamburgerException('invalid size ' + `${this._size}`);
        }
        this._size = size;

        // Hamburger stuffing exceptions processing
        if (!stuffing) {
            throw new HamburgerException('no stuffing added')
        } else if (!Hamburger.STUFFINGS.hasOwnProperty(stuffing)) {
            throw new HamburgerException('added incorrect stuffing');
        }
        this._stuffing = stuffing;

        this._topping = [];
    }

    addTopping(topping) {
        // Hamburger topping exceptions processing
        if (this._topping.includes(topping)) {
            throw new HamburgerException('duplicate topping ' + `${topping}`);
        } else {
            this._topping.push(topping);
        }
        return this._topping;
    }

    removeTopping(removeItem) {
        if (!this._topping.includes(removeItem)) {
            throw new HamburgerException('You have not add this ' + `${removeItem}` + ' before.' );
        } else {
        let index = this._topping.indexOf(removeItem);
        if (index > -1) {
            this._topping.splice(index, 1)
        }
        return this._topping;
        }
    };

    getToppings() {
        return this._topping;
    }

    getSize() {
        return this._size;
    }

    getStuffing() {
        return this._stuffing;
    }

    calculatePrice() {
        const toppingsPrice = this._topping.reduce(function (sum, item) {
            return sum + Hamburger.TOPPINGS[item].price;
        }, 0);
        const hamburgerPrice = Hamburger.SIZES[this._size].price + Hamburger.STUFFINGS[this._stuffing].price + toppingsPrice;
        return hamburgerPrice;
    }

    calculateCalories() {
        const toppingsCalories = this._topping.reduce(function (sum, item) {
            return sum + Hamburger.TOPPINGS[item].calories;
        }, 0);
        const hamburgerCalories = Hamburger.SIZES[this._size].calories + Hamburger.STUFFINGS[this._stuffing].calories + toppingsCalories;
        return hamburgerCalories;
    }
}

class HamburgerException extends Error {
    constructor(message) {
        super(message);
        this.name = 'HamburgerException'
    }
}

const SIZE_SMALL = 'SIZE_SMALL',
    SIZE_LARGE = 'SIZE_LARGE',
    STUFFING_CHEESE = 'STUFFING_CHEESE',
    STUFFING_POTATO = 'STUFFING_POTATO',
    TOPPING_MAYO = 'TOPPING_MAYO',
    TOPPING_SPICE = 'TOPPING_SPICE';


// --------------------- Examples of using -------------------------

// маленький гамбургер с начинкой из сыра
const hamburger = new Hamburger(SIZE_SMALL, STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1