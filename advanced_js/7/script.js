(function sendRequest() {
    fetch('https://swapi.dev/api/films/')
        .then(response => {
            return response.json();
        })
        .then(data => {
            distractResultsArray(data.results);
        })
})();

function distractResultsArray(resultsArray) {
    for (let i = 0; i < resultsArray.length; i++) {
        const title = resultsArray[i].title;
        const episode = resultsArray[i].episode_id;
        const openingCrawl = resultsArray[i].opening_crawl;
        const charactersList = resultsArray[i].characters;
        const newEpisode = new SWEpisode(title, episode, openingCrawl, charactersList, i)
    }
}

class SWEpisode {
    constructor(title, episode, openingCrawl, charactersList, i) {
        this.title = title;
        this.episode = episode;
        this.openingCrawl = openingCrawl;
        this.characters = charactersList;
        this.index = i;
        this._createEpisodeContainer();
    }

    _createEpisodeContainer() {
        const episodesWrapper = document.querySelector('.container');
        const container = document.createElement('div');
        container.classList.add('container__wrapper');
        episodesWrapper.append(container);
        this._createEpisodeIdBlock(container);
    }

    _createEpisodeIdBlock(container) {
        const episodeId = document.createElement('div');
        episodeId.classList.add('container__episodeId');
        episodeId.innerText = this.episode;
        container.append(episodeId);
        this._createTitleBlock(container);
    }

    _createTitleBlock(container) {
        const title = document.createElement('div');
        title.classList.add('container__title');
        container.append(title);
        const titleText = document.createElement('span');
        titleText.classList.add('container__title-text');
        titleText.innerText = this.title;
        title.append(titleText);
        this._createOpeningCrawlBlock(container);
    }

    _createOpeningCrawlBlock(container) {
        const openingCrawl = document.createElement('div');
        openingCrawl.classList.add('container__opening-crawl');
        container.append(openingCrawl);
        const openingCrawlText = document.createElement('span');
        openingCrawlText.classList.add('container__opening-crawl-text');
        openingCrawlText.innerText = this.openingCrawl;
        openingCrawl.append(openingCrawlText);
        this._createCharactersBlock(container);
    }

    _createCharactersBlock(container) {
        const characters = document.createElement('div');
        characters.classList.add('container__characters');
        const spinner = document.createElement('div');
        spinner.classList.add('loader');
        container.append(characters);
        characters.append(spinner);
        this._getListOfCharacters(this.index, spinner);
    }

    _getListOfCharacters(cardNumber, spinner) {
        this._spinner = spinner;
        let cardIndex = this.index;
        let charactersParsedArray = this.characters.map(link => {
            return fetch(`${link}`)
                .then(response => {
                    return response.json()
                })
                .then(data => {
                    return data.name;
                })
        });
        // console.log(charactersParsedArray);
        Promise.all(charactersParsedArray)
            .then(response => {
                let charactersContainer = document.querySelectorAll('.container__characters');
                let character = document.createElement('span');
                character.classList.add('container__characters-text');
                character.innerHTML = ` ${response}`;
                let containerA = charactersContainer[cardIndex];
                containerA.append(character);
                this._spinner.classList.add('disabled')
            })
    }
}

