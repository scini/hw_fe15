(function createBtn() {
    const btn = document.createElement('button');
    btn.innerText = "Detect by IP-address";
    btn.classList.add('.btn');
    document.body.append(btn);
    btn.addEventListener('click', getIp);
})();

async function getIp() {
    clearPreviousData();
    const getRequest = await fetch('https://api.ipify.org/?format=json');
    const getResponse = await getRequest.json();
    const getIp = await getResponse.ip;
    // console.log(getIp);
    sendIpRequest(getIp);
}

async function sendIpRequest(ip) {
    const getRequest = await fetch('http://ip-api.com/json/' + `${ip}?lang=ru&fields=continent,country,regionName,city,district`);
    const getResponse = await getRequest.json();
    // console.log(getResponse);
    for (let info in getResponse) {
        showUserInfo(info, getResponse)
    }
}

function showUserInfo(info, data) {
    const userData = document.createElement('p');
    userData.classList.add('user-data');
    userData.innerText = `${info + ': ' + data[info]}`;
    document.body.append(userData);
}

function clearPreviousData(){
    const printedData = document.querySelectorAll('.user-data');
    printedData.forEach(el => {
        el.remove();
    })
}


