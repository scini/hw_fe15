class List {
    posts = [];

    constructor() {
        this._createEl();
        this._addingNewPost();
    }

    _createEl() {
        this._body = document.querySelector('body');
        this._el = document.createElement('div');
        this._el.classList.add('posts');
        this._body.append(this._el);
        this._createAddButton();
        this._createModal();
    }

    _createAddButton() {
        this._addButton = document.createElement('button');
        this._addButton.classList.add('add-btn');
        this._addButton.innerText = 'Add a new post';
        this._body.append(this._addButton);
    }

    _createModal() {
        this._modalContainer = document.createElement('div');
        this._modalContainer.classList.add('modal-container');
        this._body.append(this._modalContainer);
        this._modalOverlay = document.createElement('div');
        this._modalOverlay.classList.add('modal-overlay');
        this._body.append(this._modalOverlay);
        this._saveBtn = document.createElement('button');
        this._saveBtn.classList.add('modal__button-save');
        this._saveBtn.innerText = 'Save';
        this._modalContainer.append(this._saveBtn);
        this._deleteBtn = document.createElement('button');
        this._deleteBtn.classList.add('modal__button-cancel');
        this._deleteBtn.innerText = 'Cancel';
        this._modalContainer.append(this._deleteBtn);
    }

    _addingNewPost() {
        this._addButton.onclick = () => {
            this._createModalForNewPost();
        }
    }

    _cancelAddingPost(){
        this._userName.remove();
        this._userEmail.remove();
        this._titleEdit.remove();
        this._bodyEdit.remove();
        this._modalContainer.classList.remove('opened');
        this._modalOverlay.classList.remove('opened');
    }

    _createModalForNewPost() {
        this._createModal();
        this._modalContainer.classList.add('opened');
        this._modalOverlay.classList.add('opened');
        this._userName = document.createElement('input');
        this._userName.classList.add('modal__userName-textarea');
        this._userName.placeholder = 'Your name';
        this._userName.type = 'text';
        this._userEmail = document.createElement('input');
        this._userEmail.classList.add('modal__userName-textarea');
        this._userEmail.placeholder = 'Your email';
        this._userEmail.required = true;
        this._userEmail.type = 'email';
        this._titleEdit = document.createElement('textarea');
        this._titleEdit.classList.add('modal__title-textarea');
        this._titleEdit.placeholder = 'Post title';
        this._bodyEdit = document.createElement('textarea');
        this._bodyEdit.classList.add('modal__body-textarea');
        this._bodyEdit.placeholder = 'Type your post here';
        this._modalContainer.prepend(this._bodyEdit);
        this._modalContainer.prepend(this._titleEdit);
        this._modalContainer.prepend(this._userEmail);
        this._modalContainer.prepend(this._userName);
        this._modalOverlay.addEventListener('click', (event) => {
            if (event.target === this._modalOverlay) {
                this._cancelAddingPost();
            }
        });
        this._saveBtn.onclick = () => {
            const data = {
                name: this._userName.value,
                email: this._userEmail.value,
                title: this._titleEdit.value,
                body: this._bodyEdit.value,
                id: 1,
                userId: 1
            };
            console.log(data);
            this._addNewPost(data);
            this._cancelAddingPost();
        };
        this._deleteBtn.onclick = () => {
            this._cancelAddingPost()
        }
    }

    _addNewPost(post) {
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/posts/',
            method: 'post',
            post,
            success: (response) => {
                const item = new Post({
                    post,
                    onRemove: this._removePost.bind(this),
                    onEdit: this._editPost.bind(this),
                });
                // console.log(item._post);
                this._el.prepend(item._post);
                this.posts.unshift(item);
            }
        })
    }

    appendTo(container) {
        container.append(this._el);
    }

    _removePost(item, id) {
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/posts/' + id,
            method: 'delete',
            success: () => {
                this.posts.splice(this.posts.indexOf(item), 1);
                item._deletePost();
            }
        })
    }

    _editPost(post, id, data) {
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/posts/' + id,
            method: 'put',
            data,
            success: (response) => {
                post._updateValue(response);
                post._disableEditMode();
            }
        })
    }

    addPostsFromServer(post) {
        const item = new Post({
            post,
            onRemove: this._removePost.bind(this),
            onEdit: this._editPost.bind(this),
        });
        item.appendTo(this._el);
        this.posts.push(item);
    }
}

class Post {
    constructor({post, onRemove, onEdit}) {
        this._data = post;
        this._onEdit = onEdit;
        this._onRemove = onRemove;
        this._createPostContainer();
    }

    _createPostContainer() {
        this._post = document.createElement('div');
        this._post.classList.add('post');
        this._createHeaderContainer();
        this._createTitle();
        this._createIdContainer();
    }

    _createIdContainer() {
        this._id = document.createElement('p');
        this._id.classList.add('post__id');
        this._id.innerText = `ID: ${this._data.userId}`;
        this._headerContainer.before(this._id);
    }

    _createHeaderContainer() {
        this._headerContainer = document.createElement('div');
        this._headerContainer.classList.add('post__header-container');
        this._post.prepend(this._headerContainer);
        this._addUserName();
        this._addEmail();
    }

    _addUserName() {
        this._name = document.createElement('p');
        this._name.classList.add('post__name');
        this._name.innerText = this._data.name;
        this._headerContainer.append(this._name);
    }

    _addEmail() {
        this._email = document.createElement('p');
        this._email.classList.add('post__email');
        this._email.innerText = this._data.email;
        this._headerContainer.append(this._email);
    }

    _createTitle() {
        this._titleText = document.createElement('p');
        this._titleText.classList.add('post__title');
        this._titleText.innerText = this._data.title;
        this._post.append(this._titleText);
        this._createPostText()
    }

    _createPostText() {
        this._bodyText = document.createElement('p');
        this._bodyText.classList.add('post__body');
        this._bodyText.innerText = this._data.body;
        this._post.append(this._bodyText);
        this._createBtnsContainer()
    }

    _createBtnsContainer() {
        this._btns = document.createElement('div');
        this._btns.classList.add('post__btns-container');
        this._post.append(this._btns);
        this._createEditBtn();
    }

    _createEditBtn() {
        this._editBtn = document.createElement('button');
        this._editBtn.classList.add('post__edit-btn');
        this._editBtn.innerText = 'edit';
        this._editBtn.onclick = this._enableEditMode.bind(this);
        this._btns.append(this._editBtn);
        this._createRemoveBtn();
    }

    _createRemoveBtn() {
        this._removeBtn = document.createElement('button');
        this._removeBtn.classList.add('post__remove-btn');
        this._removeBtn.innerText = 'remove';
        this._btns.append(this._removeBtn);
        this._removeBtn.onclick = () => this._onRemove(this, this._data.id);
    }

    _enableEditMode() {
        this._openModalOnEditMode();
        this._saveBtn.onclick = () => {
            const data = {
                title: this._titleEdit.value,
                body: this._bodyEdit.value
            };
            this._onEdit(this, this._data.id, data);
            this._modalWindow.classList.toggle('opened');
            this._modalWindowOverlay.classList.toggle('opened');
        };
        this._cancelBtn.onclick = () => {
            this._cancelChanges();
        }
    }

    _cancelChanges() {
        this._titleEdit.remove();
        this._bodyEdit.remove();
        this._modalWindow.classList.remove('opened');
        this._modalWindowOverlay.classList.remove('opened');
    };

    _openModalOnEditMode() {
        this._modalWindow = document.querySelector('.modal-container');
        this._modalWindow.classList.add('opened');
        this._modalWindowOverlay = document.querySelector('.modal-overlay');
        this._modalWindowOverlay.classList.add('opened');
        this._modalWindowOverlay.addEventListener('click', (event) => {
            if (event.target === this._modalWindowOverlay) {
                this._cancelChanges();
            }
        });
        this._saveBtn = document.querySelector('.modal__button-save');
        this._cancelBtn = document.querySelector('.modal__button-cancel');
        this._titleEdit = document.createElement('textarea');
        this._titleEdit.classList.add('modal__title-textarea');
        this._titleEdit.value = this._data.title;
        this._bodyEdit = document.createElement('textarea');
        this._bodyEdit.classList.add('modal__body-textarea');
        this._bodyEdit.value = this._data.body;
        this._modalWindow.prepend(this._bodyEdit);
        this._modalWindow.prepend(this._titleEdit);
    }

    _disableEditMode() {
        this._titleEdit.remove();
        this._bodyEdit.remove();
    }

    _updateValue(value) {
        this._data = {
            ...this._data,
            ...value
        };
        this._titleText.innerText = value.title;
        this._bodyText.innerText = value.body;
    }

    appendTo(container) {
        container.append(this._post);
    }

    _deletePost() {
        this._post.remove();
    }
}

const list = new List();
list.appendTo(document.body);
$.when(
    $.getJSON('https://jsonplaceholder.typicode.com/posts'),
    $.getJSON('https://jsonplaceholder.typicode.com/users')
).done(([posts], [users]) => {
    posts.forEach(post => {
        const currentUserPost = users.find(user => user.id === post.userId);
        const postWithUser = {
            ...post,
            name: currentUserPost.name,
            email: currentUserPost.email
        };
        list.addPostsFromServer(postWithUser);
    });
});
