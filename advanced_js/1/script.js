function Hamburger(size, stuffing) {
    // Hamburger size exceptions processing
    if (!size) {
        throw new HamburgerException('no size given')
    } else if (!Hamburger.SIZES.hasOwnProperty(size)) {
        throw new HamburgerException('invalid size ' + `${this.size}`);
    }
    this.size = size;

    // Hamburger stuffing exceptions processing
    if (!stuffing) {
        throw new HamburgerException('no stuffing added')
    } else if (!Hamburger.STUFFINGS.hasOwnProperty(stuffing)) {
        throw new HamburgerException('added incorrect stuffing');
    }
    this.stuffing = stuffing;
    this.topping = [];
}

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';

Hamburger.SIZES = {
    SIZE_SMALL: {
        price: 50,
        calories: 20
    },
    SIZE_LARGE: {
        price: 100,
        calories: 40
    }
};
Hamburger.STUFFINGS = {
    STUFFING_CHEESE: {
        price: 10,
        calories: 20
    },
    STUFFING_SALAD: {
        price: 20,
        calories: 5
    },
    STUFFING_POTATO: {
        price: 15,
        calories: 10
    }
};
Hamburger.TOPPINGS = {
    TOPPING_MAYO: {
        price: 20,
        calories: 5
    },
    TOPPING_SPICE: {
        price: 15,
        calories: 0
    }
};
Hamburger.prototype.addTopping = function (topping) {

    // Hamburger topping exceptions processing
    if (this.topping.includes(topping)) {
        throw new HamburgerException('duplicate topping ' + `${topping}`);
    } else {
        this.topping.push(topping);
    }
    return this.topping;
};

Hamburger.prototype.removeTopping = function (removeItem) {
    if (!this.topping.includes(removeItem)) {
        throw new HamburgerException('You have not add this ' + `${removeItem}` + ' before.' );
    } else {
        var index = this.topping.indexOf(removeItem);
        if (index > -1) {
            this.topping.splice(index, 1)
        }
        return this.topping;
    }
};

Hamburger.prototype.getToppings = function () {
    return this.topping;
};

Hamburger.prototype.getSize = function () {
    return this.size;
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

Hamburger.prototype.calculatePrice = function () {
    var toppingsPrice = this.topping.reduce(function (sum, item) {
        return sum + Hamburger.TOPPINGS[item].price;
    }, 0);
    var hamburgerPrice = Hamburger.SIZES[this.size].price + Hamburger.STUFFINGS[this.stuffing].price + toppingsPrice;
    // console.log('This hamburger costs: ' + hamburgerPrice + ' UAH');
    return hamburgerPrice;
};

Hamburger.prototype.calculateCalories = function () {
    var toppingsCalories = this.topping.reduce(function (sum, item) {
        return sum + Hamburger.TOPPINGS[item].calories;
    }, 0);
    var hamburgerCalories = Hamburger.SIZES[this.size].calories + Hamburger.STUFFINGS[this.stuffing].calories + toppingsCalories;
    // console.log('This hamburger has: ' + hamburgerCalories + ' cal');
    return hamburgerCalories;
};

function HamburgerException(message) {
    Error.call(this);
    this.message = message;
}


// --------------------- Examples of using -------------------------

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// // добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// // я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// // А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// // Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// // Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1
