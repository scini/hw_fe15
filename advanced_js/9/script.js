class AddCookies {
    constructor(button) {
        this.button = button;
        this._listener();
    }

    _listener() {
        this.button.addEventListener('click', () => {
            this._setCookieExperiment();
            this._getCookie();
        });
    }

    _setCookieExperiment() {
        let name = 'experiment';
        let value = 'novalue';
        let dateOfExpire = 300;
        document.cookie = `${name}=${value}; path=/; max-age=${dateOfExpire}`;
    }

    _setCookieNewUser(cookieName, cookieValue) {
        let name = cookieName;
        let value = cookieValue;
        document.cookie = `${name}=${value}`;
    }

    _getCookie() {
        if (document.cookie.split('new-user=true').length === 2) {
            this._setCookieNewUser('new-user', 'false')
        } else if (document.cookie.split('new-user=false').length === 2) {
            this._setCookieNewUser('new-user', 'false')
        } else {
            this._setCookieNewUser('new-user', 'true')
        }
    }
}

const contactUs = new AddCookies(document.querySelector('.btn-buy.btn-black.btn-large'));