(function sendRequest() {
    const request = new XMLHttpRequest();
    request.open('GET', 'https://swapi.dev/api/films/');
    request.addEventListener('readystatechange', function () {
        if ((request.readyState === 4) && (request.status === 200)) {
            const reqResult = JSON.parse(request.response);
            const resultsArray = reqResult.results;
            distractResultsArray(resultsArray);
        }
    });
    request.send();
})();

function distractResultsArray(resultsArray) {
    for (let i = 0; i < resultsArray.length; i++) {
        const title = resultsArray[i].title;
        const episode = resultsArray[i].episode_id;
        const openingCrawl = resultsArray[i].opening_crawl;
        const charactersList = resultsArray[i].characters;
        const newEpisode = new SWEpisode(title, episode, openingCrawl, charactersList, i)
    }
}

class SWEpisode {
    constructor(title, episode, openingCrawl, charactersList, i) {
        this.title = title;
        this.episode = episode;
        this.openingCrawl = openingCrawl;
        this.characters = charactersList;
        this.index = i;
        this._createEpisodeContainer();
    }

    _createEpisodeContainer() {
        const episodesWrapper = document.querySelector('.container');
        const container = document.createElement('div');
        container.classList.add('container__wrapper');
        episodesWrapper.append(container);
        this._createEpisodeIdBlock(container);
    }

    _createEpisodeIdBlock(container) {
        const episodeId = document.createElement('div');
        episodeId.classList.add('container__episodeId');
        episodeId.innerText = this.episode;
        container.append(episodeId);
        this._createTitleBlock(container);
    }

    _createTitleBlock(container) {
        const title = document.createElement('div');
        title.classList.add('container__title');
        container.append(title);
        const titleText = document.createElement('span');
        titleText.classList.add('container__title-text');
        titleText.innerText = this.title;
        title.append(titleText);
        this._createOpeningCrawlBlock(container);
    }

    _createOpeningCrawlBlock(container) {
        const openingCrawl = document.createElement('div');
        openingCrawl.classList.add('container__opening-crawl');
        container.append(openingCrawl);
        const openingCrawlText = document.createElement('span');
        openingCrawlText.classList.add('container__opening-crawl-text');
        openingCrawlText.innerText = this.openingCrawl;
        openingCrawl.append(openingCrawlText);
        this._createCharactersBlock(container);
    }

    _createCharactersBlock(container) {
        const characters = document.createElement('div');
        characters.classList.add('container__characters');
        container.append(characters);
        this._getListOfCharacters(this.index);
    }

    _getListOfCharacters() {
        let cardIndex = this.index;
        this.characters.forEach((link) => {
            this._getNamesOfCharacters(link, cardIndex);
        })
    }

    _getNamesOfCharacters(link, cardIndex) {
        let request = new XMLHttpRequest();
        request.open('GET', `${link}`);
        request.addEventListener('readystatechange', function () {
            if ((request.readyState === 4) && (request.status === 200)) {
                let reqResult = JSON.parse(request.response).name;
                let charactersContainer = document.querySelectorAll('.container__characters');
                let character = document.createElement('span');
                character.classList.add('container__characters-text');
                character.innerHTML = ` ${reqResult} //`;
                let containerA = charactersContainer[cardIndex];
                containerA.append(character);
            }
        });
        request.send();
    }

    _showCharactersAfterLoading(cardIndex) {
        let loadedCharacters = document.querySelectorAll('.container__characters-text');
        console.log(loadedCharacters);
    }
}

