import React, {Component} from 'react';
import basket from "../../theme/icons/2849824-basket-buy-market-multimedia-shop-shopping-store_107977.svg";

class Cart extends Component {
    render() {
        const {id, onOpenModalHandler} = this.props
        return (
            <div>
                <img className='card--cart' src={basket} alt="Add to cart"
                     onClick={() => onOpenModalHandler(id)}/>
            </div>
        );
    }
}

export default Cart;