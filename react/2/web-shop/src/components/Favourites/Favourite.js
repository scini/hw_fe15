import React, {Component} from 'react';
import '../../components/Favourites/favourite.scss'
import {ReactComponent as FavouriteItem} from "../../theme/icons/star.svg";

class Favourite extends Component {
    render() {
        const {id, favourite, onAddFavouritesHandler} = this.props
        return (
            (favourite) ?
                <FavouriteItem onClick={() => onAddFavouritesHandler(id)} className='favouriteSelected'/> :
                <FavouriteItem onClick={() => onAddFavouritesHandler(id)} className='favourite'/>
        );
    }
}

export default Favourite;