import React, {Component} from 'react';
import './button.scss';

class Button extends Component {
    render() {
        const {modalText, selectedItemId, onAddCartHandler, onOpenModalHandler} = this.props;
        const buttonStyle = {
            background: `${modalText.actions[0].background}`,
        }
        return (
            <button className='modal_button_item' style={buttonStyle}
                    onClick={() => {
                        onOpenModalHandler(modalText.id)
                        onAddCartHandler(selectedItemId)
                    }}>{modalText.actions[0].text}
            </button>
        );
    }
}

export default Button;