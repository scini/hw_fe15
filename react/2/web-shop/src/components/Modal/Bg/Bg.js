import React, {Component} from 'react';
import './bg.scss'


class Bg extends Component {
    render() {
        const {onOpenModalHandler} = this.props
        return (
            <div className="modal_background" onClick={() => onOpenModalHandler()}>
            </div>
        );
    }
}

export default Bg;