import React, {Component} from 'react';
import Button from '../Button/Button';
import './modal.scss'
import CrossButton from "../CrossButton/CrossButton";
import Bg from "./Bg/Bg";

const modalText = {
    id: 1,
    crossButton: true,
    header: 'Добавить этот товар в корзину?',
    actions: [
        {
            id: 1, background: '#000', text: 'Купить'
        },
    ]
}

class Modal extends Component {

    render() {
        const {products, selectedItemId, onAddCartHandler, onOpenModalHandler} = this.props
        const selectedItem = products.filter(item => item.id === selectedItemId)
        const crossButton = modalText.crossButton;
        return (
            <div className='modal'>
                <div className="modal_wrapper">
                    <h1 className='modal_header'>{modalText.header}</h1>
                    <div className='modal_body'>
                        <p className='modal_body-text'>{selectedItem[0].title}</p>
                        <img className='modal_approve-img' src={selectedItem[0].image} alt={selectedItem[0].title}/>
                        <div className='modal_buttons'>
                            <Button selectedItemId={selectedItem[0].id} modalText={modalText} onAddCartHandler={onAddCartHandler} onOpenModalHandler={onOpenModalHandler}/>
                        </div>
                    </div>
                    {crossButton ?
                        <CrossButton onOpenModalHandler={onOpenModalHandler}/>
                        : null}
                    <Bg onOpenModalHandler={onOpenModalHandler}/>
                </div>
            </div>
        );
    }
}

export default Modal;