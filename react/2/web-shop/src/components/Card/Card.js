import React, {Component} from 'react';
import './card.scss'
import Cart from "../Cart/Cart";
import Favourite from "../Favourites/Favourite";

class Card extends Component {

    render() {
        const {product: {id, image, number, price, color, title, favourite}, onOpenModalHandler, onAddFavouritesHandler} = this.props

        const favouriteItem = <Favourite id={id} favourite={favourite} onAddFavouritesHandler={onAddFavouritesHandler}/>
        return (
            <>
                <div className='card'>
                    <h4 className='card--header'>Робот-пылесос {title}</h4>
                    <div className='card--number-wrapper'>
                        <p className='card--product-number'>Код товара: {number}</p>
                        {favouriteItem}
                    </div>
                    <div className='card--img-wrapper'>
                        <img src={image} alt={title} className='card--img'/>
                    </div>
                    <p className='card--color'>Цвет: {color}</p>
                    <div className='card--price-wrapper'>
                        <p className='card--price'>{price.substr(0, 2) + ' ' + price.substr(2, 4)} &#8372;</p>
                        <Cart key={id} id={id} onOpenModalHandler={onOpenModalHandler}/>
                    </div>
                </div>
            </>
        );
    }
}

export default Card;