import React, {Component} from 'react';
import './crossbutton.scss';
import crossSvg from "../../theme/icons/emblemunreadable_93487.svg"

class CrossButton extends Component {
    render() {
        const {onOpenModalHandler} = this.props;
        return (
            <img src={crossSvg} alt='close' className='modal_cross-button' onClick={() => onOpenModalHandler()}/>
        );
    }
}

export default CrossButton;