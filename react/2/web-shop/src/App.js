import React, {Component} from 'react';
import './app.scss';
import axios from 'axios';
import Card from './components/Card/Card';
import Modal from "./components/Modal/Modal";

const api = axios.create({
    baseURL: 'http://localhost:3000/'
})

class App extends Component {

    state = {
        products: [],
        isModalOpen: false,
        selectedItemId: ''
    }

    componentDidMount() {
        api.get('goods.json')
            .then(response => {
                const favoritesCards = JSON.parse(localStorage.getItem("favouriteProducts"));
                let productsList = response.data;
                productsList.forEach((product) => {
                    if (favoritesCards && favoritesCards.includes(product.id)) {
                        product.favourite = true
                    }
                })
                return productsList
            })
            .then(updatedItems => {
                this.setState({products: updatedItems})
            })
    }

    onOpenModalHandler = (id) => {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
        if (id) this.setState({selectedItemId: id})
    }

    onAddFavouritesHandler = (id) => {
        const updatedListOfProducts = this.state.products;
        updatedListOfProducts.forEach(product => {
            if (product.id === id) product.favourite = !product.favourite
        })
        this.setState({products: updatedListOfProducts})
        const favouriteProducts = updatedListOfProducts.filter(product => product.favourite).map(product => product.id)
        localStorage.setItem('favouriteProducts', JSON.stringify(favouriteProducts))
    }

    onAddCartHandler = (id) => {
        const updatedListOfProducts = this.state.products;
        updatedListOfProducts.forEach(product => {
            if (product.id === id) product.cart = !product.cart
        })
        this.setState({products: updatedListOfProducts})
        const addedProducts = updatedListOfProducts.filter(product => product.cart).map(product => product.id)
        localStorage.setItem('Cart', JSON.stringify(addedProducts))
    }

    render() {
        const {products, selectedItemId, isModalOpen} = this.state
        const modal = isModalOpen ?
            <Modal products={products}
                   selectedItemId={selectedItemId}
                   onOpenModalHandler={this.onOpenModalHandler}
                   onAddCartHandler={this.onAddCartHandler}
                   isModalOpen={isModalOpen}/>
            : null;
        const productsList = products.map(product =>
            <Card
                key={product.id}
                product={product}
                selectedItemId={selectedItemId}
                products={products}
                isModalOpen={isModalOpen}
                onOpenModalHandler={this.onOpenModalHandler}
                onAddFavouritesHandler={this.onAddFavouritesHandler}/>
        )
        return (
            <div className='shop'>
                <div className='shop--product-wrapper'>
                    {productsList}
                    {modal}
                </div>
            </div>
        );
    }
}

export default App;

