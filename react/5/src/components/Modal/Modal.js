import React from 'react';
import Button from '../Button/Button';
import './modal.scss'
import CrossButton from "../CrossButton/CrossButton";
import Bg from "./Bg/Bg";
import ModalText from "./modalText";
import {openModalAction} from "../../store/Modal/modalActions";
import {useDispatch, useSelector} from "react-redux";
import {selectedIdSelector} from "../../store/Modal/modalSelectors";
import {listOfProductsSelector} from "../../store/Products/productSelectors";
import {addCartAction} from "../../store/Products/productActions";

const Modal = () => {

    const dispatch = useDispatch();
    const products = useSelector(listOfProductsSelector);
    const selectedItemId = useSelector(selectedIdSelector);
    let modalText = '';

    const selectedItem = products.filter(item => item.id === selectedItemId)

    !selectedItem[0].cart ? modalText = ModalText.addProduct : modalText = ModalText.removeProduct;
    const crossButton = modalText.crossButton;

    return (
        <div className='modal'>
            <div className="modal_wrapper">
                <h1 className='modal_header'>{modalText.header}</h1>
                <div className='modal_body'>
                    <p className='modal_body-text'>{selectedItem[0].title}</p>
                    <img className='modal_approve-img' src={selectedItem[0].image} alt={selectedItem[0].title}/>
                    <div className='modal_buttons'>
                        <Button
                            key={modalText.id}
                            id={modalText.id}
                            selectedItemId={selectedItemId}
                            selectedItem={selectedItem}
                            modalText={modalText}
                            openModalHandler={() => dispatch(openModalAction())}
                            onAddCartHandler={() => dispatch(addCartAction(selectedItemId))}/>
                    </div>
                </div>
                {crossButton ?
                    <CrossButton openModalHandler={() => dispatch(openModalAction())}/>
                    : null}
                <Bg openModalHandler={() => dispatch(openModalAction())}/>
            </div>
        </div>
    );
}


export default Modal;