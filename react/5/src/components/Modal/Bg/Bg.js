import React from 'react';
import './bg.scss'

const Bg = (props) => {
    const {openModalHandler} = props
    return (
        <div className="modal_background" onClick={() => openModalHandler()}>
        </div>
    );
}

export default Bg;