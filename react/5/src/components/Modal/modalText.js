const modalText = {
    addProduct : {
        crossButton: true,
        header: 'Add this item in your cart?',
        button: [
            {
                id: 1, background: '#1f3cc6', color: '#fff', text: 'Add'
            },
        ]
    },
    removeProduct : {
        crossButton: true,
        header: 'Delete this item from your cart?',
        button: [
            {
                id: 1, background: '#1f3cc6', color: '#fff', text: 'Delete'
            },
        ]
    }
}

export default modalText