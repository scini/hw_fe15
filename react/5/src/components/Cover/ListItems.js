import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import HomeIcon from '@material-ui/icons/Home';
import FavoriteIcon from '@material-ui/icons/Favorite';
import {Link} from "react-router-dom";

export const mainListItems = (
    <div>
        <ListItem component={Link} to="/" button>
            <ListItemIcon>
                <HomeIcon/>
            </ListItemIcon>
            <ListItemText primary="Home"/>
        </ListItem>
        <ListItem component={Link} to="/cart" button>
            <ListItemIcon>
                <ShoppingCartIcon/>
            </ListItemIcon>
            <ListItemText primary="Orders"/>
        </ListItem>
        <ListItem component={Link} to="/favourites" button>
            <ListItemIcon>
                <FavoriteIcon/>
            </ListItemIcon>
            <ListItemText primary="Favorites"/>
        </ListItem>
    </div>
);
