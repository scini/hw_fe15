import React from 'react';
import '../../components/Favourites/favourite.scss'
import {ReactComponent as FavouriteItem} from "../../theme/icons/star.svg";

const FavouriteToggle = (props) => {
    const {id, favourite, onAddFavouritesHandler} = props
        return (
            (favourite) ?
                <FavouriteItem onClick={() => onAddFavouritesHandler(id)} className='favouriteSelected'/> :
                <FavouriteItem onClick={() => onAddFavouritesHandler(id)} className='favourite'/>
        );
    }

export default FavouriteToggle;