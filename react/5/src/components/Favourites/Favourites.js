import React from 'react';
import ProductContainer from "../ProductContainer/ProductContainer";
import './favourite.scss'

const Favourites = (props) => {
    const {products, isModalOpen, selectedItemId, openModalHandler, onAddCartHandler, onAddFavouritesHandler} = props
    const favouriteProducts = [...products].filter(product => product.favourite)
    return (
        <div className='favourite_wrapper'>
            <h1 className='favourite_header'>Favorites</h1>
            <div>
                {(favouriteProducts.length !== 0) ?
                    <ProductContainer products={favouriteProducts}
                                      isModalOpen={isModalOpen}
                                      onOpenModalHandler={openModalHandler}
                                      onAddCartHandler={onAddCartHandler}
                                      onAddFavouritesHandler={onAddFavouritesHandler}
                                      selectedItemId={selectedItemId}/> :
                    <p>No favorite items :(</p>}
            </div>
        </div>
    );
}

export default Favourites;