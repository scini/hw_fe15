import React from "react";
import {Switch, Route} from 'react-router-dom';
import Cart from "../Cart/Cart";
import Favourites from "../Favourites/Favourites";
import ProductContainer from "../ProductContainer/ProductContainer";

const AppRoutes = (props) => {
    const {products, listOfCartProducts} = props
    return (
        <>
            <Switch>
                <Route exact path="/" render={() =>
                    <ProductContainer products={products}/>
                }/>
                <Route exact path='/favourites' render={() =>
                    <Favourites products={products}/>
                }/>
                <Route exact path='/cart' render={() =>
                    <Cart products={products} listOfCartProducts={listOfCartProducts}/>
                }/>
            </Switch>
        </>
    )
}

export default AppRoutes