import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from "@material-ui/core/DialogContent";
import Slide from '@material-ui/core/Slide';
import CheckOutM from "../CheckOutOrderData/CheckOutOrderData";
import {toggleCheckOutModalAction} from "../../store/CheckOutModal/checkOutActions";
import {makeStyles} from '@material-ui/core/styles';
import {useDispatch} from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: '0 auto',
        width: 450,
        height: 0,
        color: '#1f3cc6',
        textAlign: 'center',
        fontWeight: 'bold'
    }
}))

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

function CheckoutModal(props) {
    const {isCheckOutModalOpen, addedCartProducts} = props
    const dispatch = useDispatch();
    const classes = useStyles();

    const handleClose = () => {
        dispatch(toggleCheckOutModalAction())
    };

    return (
        <div>
            <Dialog
                open={isCheckOutModalOpen}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle className={classes.root}
                             id="alert-dialog-slide-title">{"Please fill in information below and confirm your order"}
                </DialogTitle>
                <DialogContent>
                    <CheckOutM addedCartProducts={addedCartProducts}/>
                </DialogContent>

            </Dialog>
        </div>
    );
};

export default CheckoutModal
