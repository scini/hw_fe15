import React from 'react';
import Card from "../Card/Card";
import {selectedIdSelector} from "../../store/Modal/modalSelectors";
import {useSelector} from "react-redux";

function ProductContainer(props) {
    const {products} = props
    const selectedItemId = useSelector(selectedIdSelector)
    const {onCart, onAddCartHandler, openModalHandler, onAddFavouritesHandler} = props
    const productList = products.map(product =>
        <Card
            key={product.id}
            product={product}
            selectedItemId={selectedItemId}
            products={products}
            onCart={onCart}
            openModalHandler={openModalHandler}
            onAddFavouritesHandler={onAddFavouritesHandler}
            onAddCartHandler={onAddCartHandler}/>
    )
    return (
        <>
            {productList}
        </>
    );
}

export default ProductContainer;