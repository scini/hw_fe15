import React from "react";
import {toggleCheckOutModalAction} from "../../store/CheckOutModal/checkOutActions";
import {useDispatch, useSelector} from "react-redux";
import {Formik, Field, Form} from 'formik';
import * as Yup from 'yup';
import Bg from '../Modal/Bg/Bg'
import {makeStyles} from '@material-ui/core/styles';
import {listOfCartProductsByLoadingSelector} from "../../store/Products/productSelectors";
import {clearCartActions} from "../../store/Products/productActions";
import {checkOutDataAction} from "../../store/CheckOutOrder/checkOutOrderActions";
import {checkOutOrderSelector} from "../../store/CheckOutOrder/checkOutOrderSelectors";
import Button from "@material-ui/core/Button";
import {TextField} from 'formik-material-ui';

const useStyles = makeStyles((theme) => ({
    root: {
        width: 550,
        height: 550,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    form: {
        marginTop: 10,
    },
    buttons_form: {
        marginTop: 15,
        display: 'flex',
        justifyContent: 'center'
    },
    button_distance_left: {
        marginRight: 30
    },
    button_distance_right: {
        marginLeft: 30
    }
}))

const CheckOutM = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cartProductsList = useSelector(listOfCartProductsByLoadingSelector);
    const buyerInformation = useSelector(checkOutOrderSelector)
    console.log('Buyer information:', buyerInformation)

    return (
        <div className='checkout'>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    email: '',
                    age: '',
                    deliveryAddress: '',
                    cellNumber: ''
                }}
                validationSchema={Yup.object().shape({
                    firstName: Yup.string()
                        .required('First Name is required'),
                    lastName: Yup.string()
                        .required('Last Name is required'),
                    email: Yup.string()
                        .email('Email is invalid')
                        .required('Email is required'),
                    age: Yup.number()
                        .moreThan(17, 'Your age should be not less than 18 years')
                        .required('Age is required'),
                    deliveryAddress: Yup.string()
                        .required('Delivery address is required'),
                    cellNumber: Yup.number()
                        .required('Cell number is required'),
                })}
                onSubmit={(fields) => {
                    dispatch(checkOutDataAction(fields));
                    dispatch(toggleCheckOutModalAction());
                    dispatch(clearCartActions(cartProductsList));
                    console.log('Order details:', cartProductsList);
                }}
                render={({errors, status, touched}) => (
                    <Form className={classes.root}>
                        <div className={classes.form}>
                            <Field component={TextField} label="First Name"
                                   helperText="Please Enter Your First Name" name="firstName" type="text"/>
                        </div>
                        <div className={classes.form}>
                            <Field component={TextField} label="Last Name" helperText="Please Enter Your Last Name"
                                   name="lastName" type="text"/>
                        </div>
                        <div className={classes.form}>
                            <Field component={TextField} label="Email"
                                   helperText="Please Enter Email" name="email" type="text"/>
                        </div>
                        <div className={classes.form}>
                            <Field component={TextField} label="Age"
                                   helperText="Please Enter Your Age" name="age" type="text"/>
                        </div>
                        <div className={classes.form}>
                            <Field component={TextField} label="Delivery Address"
                                   helperText="Please Enter Your Delivery Address" name="deliveryAddress" type="text"/>
                        </div>
                        <div className={classes.form}>
                            <Field component={TextField} label="Cell Number"
                                   helperText="Please Enter Your Cell Number" name="cellNumber" type="text"/>
                        </div>
                        <div className={classes.buttons_form}>
                            <div className={classes.button_distance_left}>
                                <Button variant="contained" color="primary" type="submit"
                                        className="btn btn-primary mr-2">Confirm order</Button>
                            </div>
                            <div>
                                <Button variant="contained" color="primary" type="reset"
                                        className="btn btn-secondary">Reset</Button>
                            </div>
                        </div>
                    </Form>
                )}
            />
            <Bg openModalHandler={() => dispatch(toggleCheckOutModalAction())}/>
        </div>
    )
}

export default CheckOutM