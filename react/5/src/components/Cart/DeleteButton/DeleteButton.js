import React from 'react';
import "./deleteButton.scss"

function DeleteButton(props) {
    const {id, openModalHandler} = props
    return (
        <button onClick={() => openModalHandler(id)} className='delete-button'>X</button>
    );
}

export default DeleteButton;