import React from 'react';
import ProductContainer from "../ProductContainer/ProductContainer";
import "./cart.scss"
import {useDispatch, useSelector} from "react-redux";
import {checkOutModalSelector} from "../../store/CheckOutModal/checkOutSelectors";
import {toggleCheckOutModalAction} from "../../store/CheckOutModal/checkOutActions";
import CheckoutModal from "../CheckoutModal/CheckoutModal";
import Button from '@material-ui/core/Button';

const Cart = (props) => {
    const {listOfCartProducts, selectedItemId, onAddCartHandler, onAddFavouritesHandler} = props

    const onCart = true;

    const dispatch = useDispatch();
    const isCheckOutModalOpen = useSelector(checkOutModalSelector);
    const checkOutModal = isCheckOutModalOpen ? <CheckoutModal addedCartProducts = {listOfCartProducts}  isCheckOutModalOpen={isCheckOutModalOpen}/> : null

    return (
        <div className='cart_product-wrapper'>
            <h1 className='cart_heading'>Cart</h1>
            {checkOutModal}
            <div>
                {
                    (listOfCartProducts && listOfCartProducts.length !== 0) ?
                    <ProductContainer products={listOfCartProducts}
                                      onCart={onCart}
                                      onAddCartHandler={onAddCartHandler}
                                      onAddFavouritesHandler={onAddFavouritesHandler}
                                      selectedItemId={selectedItemId}/> :
                    <p>Your cart is empty :(</p>
                }
            </div>
            <div className='button_checkOut' >
                {
                    (listOfCartProducts && listOfCartProducts.length !== 0) ?
                        <Button variant="contained" color="primary" onClick={()=>dispatch(toggleCheckOutModalAction())} disableElevation>Check Out</Button> :
                    null
                }
            </div>
        </div>
    );
}

export default Cart;