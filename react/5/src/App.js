import React, {useEffect} from 'react';
import './app.scss';
import Modal from "./components/Modal/Modal";
import {loadProductsAction} from "./store/Products/productActions";
import {useDispatch, useSelector} from "react-redux";
import {listOfCartProductsByLoadingSelector, listOfProductsSelector} from "./store/Products/productSelectors";
import {openModalSelector} from "./store/Modal/modalSelectors";
import Main from "./components/Cover/Cover";

const App = () => {

    const dispatch = useDispatch();
    const products = useSelector(listOfProductsSelector)
    const listOfCartProducts = useSelector(listOfCartProductsByLoadingSelector)
    const isModalOpen = useSelector(openModalSelector);


    useEffect(() => {
            dispatch(loadProductsAction());
            }, [dispatch])

     const modal = isModalOpen ? <Modal /> : null;

    return (
        <div className='shop'>
            <Main listOfCartProducts={listOfCartProducts} products={products}/>
            {modal}
        </div>
    );
}

export default App;

