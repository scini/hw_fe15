import axios from 'axios';
import * as actions from '../actionTypes';

export const loadProductsAction = () => (dispatch) => {
    axios.get('http://localhost:3000/goods.json')
        .then(response => {
            const favoriteCards = JSON.parse(localStorage.getItem("favouriteProducts"));
            const selectedGoods = JSON.parse(localStorage.getItem("Cart"));

            let productsList = response.data;
            productsList.forEach((product) => {
                if (favoriteCards && favoriteCards.includes(product.id)) {
                    product.favourite = true
                }
                if (selectedGoods && selectedGoods.includes(product.id)) {
                    product.cart = true
                }
            })
            return productsList
        })
        .then(productsList => {
            dispatch({
                type: actions.SAVE_PRODUCTS,
                payload: productsList});
        })

}

export const addCartAction = (id) => (dispatch) => {
    dispatch({
        type: actions.TOGGLE_CART,
        payload: id
    })
}

export const clearCartActions = (cartProductsList) => (dispatch) => dispatch({
    type: actions.CLEAR_CART,
    payload: cartProductsList
})

export const addFavoriteAction = (id) => (dispatch) => {
    dispatch({
        type: actions.TOGGLE_FAVORITE,
        payload: id
    })
}




