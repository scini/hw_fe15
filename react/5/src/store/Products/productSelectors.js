export const listOfProductsSelector = (store) => store.productsList.products
export const listOfCartProductsByLoadingSelector = (store) => store.productsList.productsInCart
