import * as actions from '../actionTypes'

const initialStore = {
    products: [],
    productsInCart: [],
}

const productsReducer = (store = initialStore, action) => {

    switch (action.type) {

        case actions.SAVE_PRODUCTS:
            const productsByLoading = action.payload.filter(product => product.cart)
            return {
                ...store,
                products: action.payload,
                productsInCart: productsByLoading,
            }

        case actions.TOGGLE_CART:
            const updatedListOfCartProducts = [...store.products];
            updatedListOfCartProducts.forEach(product => {
                if (product.id === action.payload) product.cart = !product.cart
            })
            const addedCartProducts = updatedListOfCartProducts.filter(product => product.cart)
            const addedCartProductsIndex = addedCartProducts.map(product => product.id)
            localStorage.setItem('Cart', JSON.stringify(addedCartProductsIndex))
            return {
                ...store,
                productsInCart: addedCartProducts
            }

        case actions.CLEAR_CART:
            const checkedOutProducts = action.payload;
            checkedOutProducts.forEach(product => {
                product.cart = !product.cart
            })
            const removedProducts = checkedOutProducts.filter(product => product.cart)
            const removedProductsLocalStorage = removedProducts.map(product => product.id)
            localStorage.setItem('Cart', JSON.stringify(removedProductsLocalStorage))
            return {
                ...store,
                productsInCart: removedProducts
            }

        case actions.TOGGLE_FAVORITE:
            const updatedListOfFavoriteProducts = [...store.products];
            updatedListOfFavoriteProducts.forEach(product => {
                if (product.id === action.payload) product.favourite = !product.favourite
            })
            const favoriteProducts = updatedListOfFavoriteProducts.filter(product => product.favourite).map(product => product.id)
            localStorage.setItem('favouriteProducts', JSON.stringify(favoriteProducts))
            return {
                ...store,
                products: updatedListOfFavoriteProducts
            }

        default:
            return store
    }
}

export default productsReducer