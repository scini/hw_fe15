import * as actions from "../actionTypes";

const initialStore = {
    isModalOpen: false,
    selectedItemId: null
}

const modalReducer = (store = initialStore, action) => {
    const id = action.payload ? action.payload : null;
    switch (action.type) {
        case actions.OPEN_MODAL:
            return {
                ...store,
                isModalOpen: !store.isModalOpen,
                selectedItemId: id
            }
        default:
            return store;
    }
}

export default modalReducer