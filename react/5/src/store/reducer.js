import {combineReducers} from "redux";
import modalReducer from "./Modal/modalReducer";
import productsReducer from "./Products/productsReducer";
import checkOutReducer from "./CheckOutModal/checkOutReducer";
import checkOutUserData from "./CheckOutOrder/checkOutOrderReducer";

const reducer = combineReducers({
    openModal: modalReducer,
    productsList: productsReducer,
    checkOutModal: checkOutReducer,
    buyerData: checkOutUserData,
})

export default reducer