import * as actions from '../actionTypes'

export const toggleCheckOutModalAction = () => dispatch => dispatch ({
    type: actions.TOGGLE_CHECKOUT
})

