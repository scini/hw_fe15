import * as actions from "../actionTypes";

const initialStore = {
    isModalOpen: false,
    selectedItemId: null
}
const checkOutReducer = (store = initialStore, action) => {
    switch (action.type) {
        case actions.TOGGLE_CHECKOUT:
            return {
                ...store,
                isModalOpen: !store.isModalOpen
            }
        default:
            return store;
    }
}

export default checkOutReducer