import * as actions from '../actionTypes'

const initialStore = {
    buyerInformation: {},
}

const checkOutUserData = (store = initialStore, action) => {
    switch (action.type) {
        case actions.ADD_PRODUCTS:
            return {
                ...store,
                buyerInformation: action.payload,
            }

        default:
            return store
    }
}

export default checkOutUserData