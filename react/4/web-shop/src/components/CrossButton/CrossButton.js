import React from 'react';
import './crossbutton.scss';
import crossSvg from "../../theme/icons/emblemunreadable_93487.svg"

const CrossButton = (props) => {
    const {openModalHandler} = props;
    return (
        <img src={crossSvg} alt='close' className='modal_cross-button' onClick={() => openModalHandler()}/>
    );
}

export default CrossButton;