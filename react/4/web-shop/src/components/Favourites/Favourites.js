import React from 'react';
import Body from "../../containers/Body/Body";
import './favourite.scss'

const Favourites = (props) => {
    const {products, isModalOpen, selectedItemId, openModalHandler, onAddCartHandler, onAddFavouritesHandler} = props
    const favouriteProducts = [...products].filter(product => product.favourite)
    return (
        <div className='favourite_wrapper'>
            <h1 className='favourite_header'>Избранное</h1>
            <div>
                {(favouriteProducts.length !== 0) ?
                    <Body products={favouriteProducts}
                          isModalOpen={isModalOpen}
                          onOpenModalHandler={openModalHandler}
                          onAddCartHandler={onAddCartHandler}
                          onAddFavouritesHandler={onAddFavouritesHandler}
                          selectedItemId={selectedItemId}/> :
                    <p>В избранном нет товаров</p>}
            </div>
        </div>
    );
}

export default Favourites;