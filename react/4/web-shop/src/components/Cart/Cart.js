import React from 'react';
import Body from "../../containers/Body/Body";
import "./cart.scss"

const Cart = (props) => {
    const {products, selectedItemId, onAddCartHandler, onAddFavouritesHandler} = props
    const addedCartProducts = [...products].filter(product => product.cart)
    const onCart = true;
    return (
        <div className='cart_product-wrapper'>
            <h1 className='cart_heading'>Корзина</h1>
            <div>
                {(addedCartProducts.length !== 0) ?
                    <Body products={addedCartProducts}
                          onCart={onCart}
                          onAddCartHandler={onAddCartHandler}
                          onAddFavouritesHandler={onAddFavouritesHandler}
                          selectedItemId={selectedItemId}/> :
                    <p>Ваша корзина пуста</p>}
            </div>
        </div>
    );
}

export default Cart;