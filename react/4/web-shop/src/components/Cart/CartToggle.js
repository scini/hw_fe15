import React from 'react';
import '../../components/Cart/cart.scss'
import DeleteFromCart from "../../theme/icons/1486504830-delete-dustbin-empty-recycle-recycling-remove-trash_81361.png";
import {ReactComponent as Basket} from "../../theme/icons/2849824-basket-buy-market-multimedia-shop-shopping-store_107977.svg";

const CartToggle = (props) => {
    const {id, cart, openModalHandler} = props
    return (
        (!cart) ?
            <Basket id={id} onClick={() => openModalHandler(id)} className='cart-delete'/> :
            <img id={id} className='cart' src={DeleteFromCart} alt='Add to cart' onClick={() => openModalHandler(id)}/>
    );
}

export default CartToggle;