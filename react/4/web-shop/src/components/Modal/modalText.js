const modalText = {
    addProduct : {
        crossButton: true,
        header: 'Добавить этот товар в корзину?',
        button: [
            {
                id: 1, background: '#000', text: 'Добавить'
            },
        ]
    },
    removeProduct : {
        crossButton: true,
        header: 'Удалить этот товар из корзины?',
        button: [
            {
                id: 1, background: '#4f2121', text: 'Удалить'
            },
        ]
    }
}

export default modalText