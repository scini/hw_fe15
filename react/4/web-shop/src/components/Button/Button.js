import React from 'react';
import './button.scss';

const Button = (props) => {
    const {modalText, openModalHandler, selectedItemId, onAddCartHandler} = props;
    const buttonStyle = {
        background: `${modalText.button[0].background}`,
    }
    return (
        <button className='modal_button_item' style={buttonStyle}
                onClick={() => {
                    openModalHandler(selectedItemId)
                    onAddCartHandler(selectedItemId)
                }}
        >{modalText.button[0].text}
        </button>
    );
}

export default Button;