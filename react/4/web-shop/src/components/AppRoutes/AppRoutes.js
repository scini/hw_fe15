import React from "react";
import {Switch, Route} from 'react-router-dom';
import Cart from "../Cart/Cart";
import Favourites from "../Favourites/Favourites";
import Body from "../../containers/Body/Body";

const AppRoutes = (props) => {
    const {products} = props
    return (
        <>
            <Switch>
                <Route exact path="/" render={() =>
                    <Body products={products}/>
                }/>
                <Route exact path='/favourites' render={() =>
                    <Favourites products={products}/>
                }/>
                <Route exact path='/cart' render={() =>
                    <Cart products={products}/>
                }/>
            </Switch>
        </>
    )
}

export default AppRoutes