import {combineReducers} from "redux";
import modalReducer from "./Modal/modalReducer";
import productsReducer from "./Products/productsReducer";

const reducer = combineReducers({
    openModal: modalReducer,
    productsList: productsReducer
})

export default reducer