import * as actions from '../actionTypes'

const initialStore = {
    products: []
}

const productsReducer = (store = initialStore, action) => {

    switch (action.type) {

        case actions.SAVE_PRODUCTS:
            return {
                ...store,
                products: action.payload}

        case actions.TOGGLE_CART:
            const updatedListOfCartProducts = [...store.products];
            updatedListOfCartProducts.forEach(product => {
                if (product.id === action.payload) product.cart = !product.cart
            })
            const addedProducts = updatedListOfCartProducts.filter(product => product.cart).map(product => product.id)
            localStorage.setItem('Cart', JSON.stringify(addedProducts))
            return  {
                ...store,
                products: updatedListOfCartProducts
            }

        case actions.TOGGLE_FAVORITE:
            const updatedListOfFavoriteProducts = [...store.products];
            updatedListOfFavoriteProducts.forEach(product => {
                if (product.id === action.payload) product.favourite = !product.favourite
            })
            const favoriteProducts = updatedListOfFavoriteProducts.filter(product => product.favourite).map(product => product.id)
            localStorage.setItem('favouriteProducts', JSON.stringify(favoriteProducts))
            return {
                ...store,
                products: updatedListOfFavoriteProducts
            }

        default:
            return store
    }
}

export default productsReducer