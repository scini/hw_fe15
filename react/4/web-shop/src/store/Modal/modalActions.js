import * as actions from '../actionTypes';

export const openModalAction = (id) => (dispatch) => dispatch({
    type: actions.OPEN_MODAL,
    payload: id

})

