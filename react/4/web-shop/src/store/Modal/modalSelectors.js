export const openModalSelector = (store) => store.openModal.isModalOpen;
export const selectedIdSelector = (store) => store.openModal.selectedItemId;