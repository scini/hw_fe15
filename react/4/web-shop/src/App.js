import React, {useEffect} from 'react';
import './app.scss';
import Modal from "./components/Modal/Modal";
import Header from "./containers/Header/Header";
import AppRoutes from "./components/AppRoutes/AppRoutes";
import {loadProductsAction} from "./store/Products/productActions";
import {useDispatch, useSelector} from "react-redux";
import {listOfProductsSelector} from "./store/Products/productSelectors";
import {openModalSelector} from "./store/Modal/modalSelectors";

const App = () => {

    const dispatch = useDispatch();
    const products = useSelector(listOfProductsSelector)
    const isModalOpen = useSelector(openModalSelector);


    useEffect(() => {
            dispatch(loadProductsAction())
    }, [dispatch])

     const modal = isModalOpen ? <Modal /> : null;

    return (
        <div className='shop'>
            <Header/>
            <div className='shop--product-wrapper'>
                {modal}
                <AppRoutes products={products}/>
            </div>
        </div>
    );
}

export default App;

