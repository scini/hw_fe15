import React from 'react'
import NavBar from "./NavBar/NawBar";
import "./header.scss"

const Header = () => {
    return (
        <div>
            <h1 className='header'>Vacuum Cleaners</h1>
            <NavBar/>
        </div>
    )
}

export default Header