import React from 'react';
import Card from "../../components/Card/Card";
import {selectedIdSelector} from "../../store/Modal/modalSelectors";
import {useSelector} from "react-redux";

function Body(props) {
    const {products} = props
    console.log(products)
    const selectedItemId = useSelector(selectedIdSelector)
    const {onCart, onAddCartHandler, openModalHandler, onAddFavouritesHandler} = props
    const productList = products.map(product =>
            <Card
                key={product.id}
                product={product}
                selectedItemId={selectedItemId}
                products={products}
                onCart={onCart}
                openModalHandler={openModalHandler}
                onAddFavouritesHandler={onAddFavouritesHandler}
                onAddCartHandler={onAddCartHandler}/>
                )
    return (
        <>
            {productList}
        </>
    );
}

export default Body;