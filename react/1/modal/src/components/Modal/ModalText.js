const modals = [
    {
        id: 1,
        crossButton: false,
        header: 'You are going to overwrite this file',
        text: "Please click 'Yes' if you are sure you want to overwrite this file or 'No' if you don't! ?",
        actions: [
            {
                id: 1, background: 'blue', text: 'Yes'
            },
            {
                id: 2, background: 'blue', text: 'No'
            }
        ]
    },
    {
        id: 2,
        crossButton: true,
        header: 'Do you want to delete this file?',
        text: 'Once you delete this file, it will not be possible to undo this action. Are you sure you want to delete?',
        actions: [
            {
                id: 1, background: '#B3382C', text: 'Ok'
            },
            {
                id: 2, background: '#B3382C', text: 'Cancel'
            }
        ]
    },
];

export default modals