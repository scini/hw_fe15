import React, {PureComponent} from 'react';
import './bg.scss'
import PropTypes from 'prop-types';


class Bg extends PureComponent {
    render() {
        const {onClickHandler} = this.props
        return (
            <div className="modal_background" onClick={() => onClickHandler()}>
            </div>
        );
    }
}

Bg.propTypes = {
    onClickHandler:PropTypes.func
}

export default Bg;