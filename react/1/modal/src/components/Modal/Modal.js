import React, {PureComponent} from 'react';
import './modal.scss';
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import CrossButton from "../Button/CrossButton";
import Bg from "./ModalBg/Bg";

class Modal extends PureComponent {

    render() {
        const {mainParametres, actions, onCloseHandler} = this.props
        const modalButtons = actions.map(button => {
            const {id} = button
            return (
                <Button key={id} id={id} button={button} isOpen onClickHandler={onCloseHandler}/>
            )
        })
        const {crossButton} = mainParametres

        return (
            <div className='modal'>
                <div className="modal_wrapper">
                    <h1 className='modal_header'>{mainParametres.header}</h1>
                    <div className='modal_body'>
                        <p className='modal_body-text'>{mainParametres.text}</p>
                        <div className='modal_buttons'>
                            {modalButtons}
                        </div>
                    </div>
                    {crossButton ?
                        <CrossButton onClickHandler={onCloseHandler}/>
                        : null}
                    <Bg onClickHandler={onCloseHandler}/>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    mainParametres: PropTypes.object,
    actions: PropTypes.array,
    isOpen: PropTypes.bool,
    onCloseHandler: PropTypes.func,
}


export default Modal;