import React, {PureComponent} from 'react';
import './button.scss';
import PropTypes from 'prop-types';

class Button extends PureComponent {
    render() {
        const {button, id, onClickHandler} = this.props;
        const buttonStyle = {
            background: `${button.background}`,
        }
        return (
            <button className='modal_button_item' style={buttonStyle}
                    onClick={() => onClickHandler(id)}>{button.text}
            </button>
        );
    }
}

Button.propTypes = {
    id: PropTypes.number,
    onCloseHandler: PropTypes.func,
    button: PropTypes.object,
}

export default Button;