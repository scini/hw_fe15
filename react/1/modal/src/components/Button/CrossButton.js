import React, {PureComponent} from 'react';
import crossSvg from "../Button/crosshd_106070.svg"
import PropTypes from 'prop-types';
import './crossbutton.scss'

class CrossButton extends PureComponent {
    render() {
        const {onClickHandler} = this.props;
        return (
            <img src={crossSvg} alt='close' className='modal_cross-button' onClick={() => onClickHandler()}/>
        );
    }
}

CrossButton.propTypes = {
    onCloseHandler: PropTypes.func,
}

export default CrossButton;