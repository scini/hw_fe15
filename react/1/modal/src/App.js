import React, {PureComponent} from 'react';
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import modals from "./components/Modal/ModalText";

class App extends PureComponent {
    constructor(props) {
        super(props);
        this.buttons = [
            {id: 1, background: 'green', text: 'Open first modal'},
            {id: 2, background: 'blue', text: 'Open second modal'},
        ];
        this.temporaryModal = '';
        this.state = {
            isOpen: false
        }
    }

    onOpenHandler = (id) => {
        this.setState({
            isOpen: !this.state.isOpen
        })
        this.temporaryModal = modals[id - 1]
    }

    onCloseHandler = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    render() {
        const {id} = modals
        const buttons = this.buttons.map(button => {
            const {id} = button
            return (
                <Button key={id} id={id} button={button} onClickHandler={this.onOpenHandler}/>
            )
        });

        return (
            <div className="App">
                {
                    this.state.isOpen ?
                        <Modal key={id}
                               mainParametres={this.temporaryModal}
                               actions={this.temporaryModal.actions} onCloseHandler={this.onCloseHandler}/>
                               :
                        null
                }
                {buttons}
            </div>
        )
    }
}

export default App;
