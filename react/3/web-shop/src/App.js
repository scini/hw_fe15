import React, {useState, useEffect} from 'react';
import './app.scss';
import axios from 'axios';
import Modal from "./components/Modal/Modal";
import Header from "./containers/Header/Header";
import AppRoutes from "./components/AppRoutes/AppRoutes";

const api = axios.create({
    baseURL: 'http://localhost:3000/'
})

const App = () => {
    const [products, setProducts] = useState([])
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [selectedItemId, setSelectedItemId] = useState('')

    const onOpenModalHandler = (id) => {
        setIsModalOpen(!isModalOpen)
        if (id) setSelectedItemId(id)
    }

    useEffect(() => {
        api.get('goods.json')
            .then(response => {
                const favoriteCards = JSON.parse(localStorage.getItem("favouriteProducts"));
                const selectedGoods = JSON.parse(localStorage.getItem("Cart"));

                let productsList = response.data;
                productsList.forEach((product) => {
                    if (favoriteCards && favoriteCards.includes(product.id)) {
                        product.favourite = true
                    }
                    if (selectedGoods && selectedGoods.includes(product.id)) {
                        product.cart = true
                    }
                })
                return productsList
            })
            .then(updatedItems => setProducts(updatedItems)
            )
    }, [])

    const onAddFavouritesHandler = (id) => {
        const updatedListOfProducts = [...products];
        updatedListOfProducts.forEach(product => {
            if (product.id === id) product.favourite = !product.favourite
        })
        setProducts(updatedListOfProducts)
        const favouriteProducts = updatedListOfProducts.filter(product => product.favourite).map(product => product.id)
        localStorage.setItem('favouriteProducts', JSON.stringify(favouriteProducts))
    }

    const onAddCartHandler = (id) => {
        const updatedListOfProducts = [...products];
        updatedListOfProducts.forEach(product => {
            if (product.id === id) product.cart = !product.cart
        })
        setProducts(updatedListOfProducts)
        const addedProducts = updatedListOfProducts.filter(product => product.cart).map(product => product.id)
        localStorage.setItem('Cart', JSON.stringify(addedProducts))
    }

    const modal = isModalOpen ?
        <Modal products={products}
               selectedItemId={selectedItemId}
               onOpenModalHandler={onOpenModalHandler}
               onAddCartHandler={onAddCartHandler}
               isModalOpen={isModalOpen}/>
        : null;

    return (
        <div className='shop'>
            <Header/>
            <div className='shop--product-wrapper'>
                {modal}
                <AppRoutes products={products}
                           selectedItemId={selectedItemId}
                           onOpenModalHandler={onOpenModalHandler}
                           onAddCartHandler={onAddCartHandler}
                           onAddFavouritesHandler={onAddFavouritesHandler}
                           isModalOpen={isModalOpen}/>
            </div>
        </div>
    );
}

export default App;

