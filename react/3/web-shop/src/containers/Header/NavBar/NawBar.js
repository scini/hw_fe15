import React from "react";
import {NavLink} from "react-router-dom";
import "./NavBar.scss"

const NavBar = () => {
    return (
        <nav>
            <ul className='nav_bar'>
                <li className='nav_bar-list'>
                    <NavLink to="/" exact activeClassName='active'>Home</NavLink>
                </li>
                <li className='nav_bar-list'>
                    <NavLink to="/cart" activeClassName='active'>Cart</NavLink>
                </li>
                <li className='nav_bar-list'>
                    <NavLink to="/favourites" activeClassName='active'>Favourites</NavLink>
                </li>

            </ul>
        </nav>
    )
}

export default NavBar