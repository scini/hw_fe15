import React from 'react';
import Card from "../../components/Card/Card";

function Body(props) {
    const {products, selectedItemId, isModalOpen, onCart, onAddCartHandler, onOpenModalHandler, onAddFavouritesHandler} = props
    const productList = products.map(product =>
            <Card
                key={product.id}
                product={product}
                selectedItemId={selectedItemId}
                products={products}
                onCart={onCart}
                isModalOpen={isModalOpen}
                onOpenModalHandler={onOpenModalHandler}
                onAddFavouritesHandler={onAddFavouritesHandler}
                onAddCartHandler={onAddCartHandler}/>
                )
    return (
        <>
            {productList}
        </>
    );
}

export default Body;