import React from 'react';
import './card.scss'
import FavouriteToggle from "../Favourites/FavouriteToggle";
import CartToggle from "../Cart/CartToggle";
import DeleteButton from "../Cart/DeleteButton/DeleteButton";

const Card = (props) => {
    const {product: {id, image, number, price, color, title, favourite, cart}, onCart, onOpenModalHandler, onAddCartHandler, onAddFavouritesHandler} = props
    const favouriteItem = <FavouriteToggle id={id} favourite={favourite} onAddFavouritesHandler={onAddFavouritesHandler}/>
    const cartAddedItem = <CartToggle id={id} cart={cart} onOpenModalHandler={onOpenModalHandler} onAddCartHandler={onAddCartHandler}/>
    return (
        <>
            <div className='card'>
                {onCart ? <DeleteButton id={id} onOpenModalHandler={onOpenModalHandler}/> : null}
                <h4 className='card--header'>Робот-пылесос {title}</h4>
                <div className='card--number-wrapper'>
                    <p className='card--product-number'>Код товара: {number}</p>
                    {favouriteItem}
                </div>
                <div className='card--img-wrapper'>
                    <img src={image} alt={title} className='card--img'/>
                </div>
                <p className='card--color'>Цвет: {color}</p>
                <div className='card--price-wrapper'>
                    <p className='card--price'>{price.substr(0, 2) + ' ' + price.substr(2, 4)} &#8372;</p>
                    {cartAddedItem}
                </div>
            </div>
        </>
    );

}

export default Card;