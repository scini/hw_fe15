import React from 'react';
import "./deleteButton.scss"

function DeleteButton(props) {
    const {id, onOpenModalHandler} = props
    return (
        <button onClick={() => onOpenModalHandler(id)} className='delete-button'>X</button>
    );
}

export default DeleteButton;