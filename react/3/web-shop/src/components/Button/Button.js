import React from 'react';
import './button.scss';

const Button = (props) => {
    const {modalText, selectedItemId, onAddCartHandler, onOpenModalHandler} = props;
    const buttonStyle = {
        background: `${modalText.button[0].background}`,
    }
    return (
        <button className='modal_button_item' style={buttonStyle}
                onClick={() => {
                    onOpenModalHandler(modalText.id)
                    onAddCartHandler(selectedItemId)
                }}>{modalText.button[0].text}
        </button>
    );
}

export default Button;