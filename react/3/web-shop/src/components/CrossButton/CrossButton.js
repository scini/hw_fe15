import React from 'react';
import './crossbutton.scss';
import crossSvg from "../../theme/icons/emblemunreadable_93487.svg"

const CrossButton = (props) => {
    const {onOpenModalHandler} = props;
    return (
        <img src={crossSvg} alt='close' className='modal_cross-button' onClick={() => onOpenModalHandler()}/>
    );
}

export default CrossButton;