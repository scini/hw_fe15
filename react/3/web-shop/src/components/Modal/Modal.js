import React from 'react';
import Button from '../Button/Button';
import './modal.scss'
import CrossButton from "../CrossButton/CrossButton";
import Bg from "./Bg/Bg";
import ModalText from "./modalText"

const Modal = (props) => {
    let modalText = '';
    const {products, selectedItemId, onOpenModalHandler, onAddCartHandler} = props
    const selectedItem = products.filter(item => item.id === selectedItemId)
    !selectedItem[0].cart ? modalText = ModalText.addProduct : modalText = ModalText.removeProduct;
    const crossButton = modalText.crossButton;

    return (
        <div className='modal'>
            <div className="modal_wrapper">
                <h1 className='modal_header'>{modalText.header}</h1>
                <div className='modal_body'>
                    <p className='modal_body-text'>{selectedItem[0].title}</p>
                    <img className='modal_approve-img' src={selectedItem[0].image} alt={selectedItem[0].title}/>
                    <div className='modal_buttons'>
                        <Button
                            key={modalText.id}
                            id={modalText.id}
                            selectedItemId={selectedItemId}
                            selectedItem={selectedItem}
                            modalText={modalText}
                            onOpenModalHandler={onOpenModalHandler}
                            onAddCartHandler={onAddCartHandler}/>
                    </div>
                </div>
                {crossButton ?
                    <CrossButton onOpenModalHandler={onOpenModalHandler}/>
                    : null}
                <Bg onOpenModalHandler={onOpenModalHandler}/>
            </div>
        </div>
    );
}

export default Modal;