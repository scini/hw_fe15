import React from 'react';
import './bg.scss'


const Bg = (props) => {
    const {onOpenModalHandler} = props
    return (
        <div className="modal_background" onClick={() => onOpenModalHandler()}>
        </div>
    );
}

export default Bg;