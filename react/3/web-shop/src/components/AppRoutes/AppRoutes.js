import React from "react";
import {Switch, Route} from 'react-router-dom';
import Cart from "../Cart/Cart";
import Favourites from "../Favourites/Favourites";
import Body from "../../containers/Body/Body";

const AppRoutes = (props) => {
    const {products, isModalOpen, selectedItemId, onAddCartHandler, onOpenModalHandler, onAddFavouritesHandler} = props
    return (
        <>
            <Switch>
                <Route exact path="/" render={() =>
                    <Body products={products}
                          selectedItemId={selectedItemId}
                          onOpenModalHandler={onOpenModalHandler}
                          onAddFavouritesHandler={onAddFavouritesHandler}
                          onAddCartHandler={onAddCartHandler}
                          isModalOpen={isModalOpen}/>
                }/>
                <Route exact path='/favourites' render={() =>
                    <Favourites products={products}
                                selectedItemId={selectedItemId}
                                onOpenModalHandler={onOpenModalHandler}
                                onAddFavouritesHandler={onAddFavouritesHandler}
                                onAddCartHandler={onAddCartHandler}
                                isModalOpen={isModalOpen}/>
                }/>
                <Route exact path='/cart' render={() =>
                    <Cart products={products}
                          selectedItemId={selectedItemId}
                          onOpenModalHandler={onOpenModalHandler}
                          onAddFavouritesHandler={onAddFavouritesHandler}
                          onAddCartHandler={onAddCartHandler}
                          isModalOpen={isModalOpen}/>
                }/>
            </Switch>
        </>
    )
}

export default AppRoutes